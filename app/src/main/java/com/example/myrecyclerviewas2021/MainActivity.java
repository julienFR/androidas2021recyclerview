package com.example.myrecyclerviewas2021;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.myrecyclerviewas2021.Model.Contact;
import com.example.myrecyclerviewas2021.Tools.MyAdapter;
import com.example.myrecyclerviewas2021.Tools.MyViewHolder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Contact> contacts = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MyAdapter myAdapter;
    private Parcelable recyclerState;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contacts = Contact.genererContacts(20);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        if (sharedPreferences.contains("lescontacts")){
            contacts = new Gson().fromJson(sharedPreferences.getString("lescontacts",null),
                    new TypeToken<List<Contact>>(){}.getType());
        }
        myAdapter = new MyAdapter(contacts);
        recyclerView.setAdapter(myAdapter);

    }

    public void ajouter(View view) {
        startActivityForResult(new Intent(this,Main2Activity.class),1);
    }

    @Override
    public void onActivityResult(int requestcode,int resultcode,Intent intent){
        super.onActivityResult(requestcode,resultcode,intent);
        if (requestcode == 1){
            if (resultcode == Activity.RESULT_OK){
                Log.d("MesLogs","onactivityresult");
                Contact c = intent.getParcelableExtra("contact");
                contacts.add(c);
                myAdapter.notifyDataSetChanged();
            }else{
                Toast.makeText(this,"pb ajout contact",Toast.LENGTH_LONG).show();
            }
        }
    }

    protected void onSaveInstanceState(Bundle state) {

        super.onSaveInstanceState(state);
        recyclerState = linearLayoutManager.onSaveInstanceState();
        state.putParcelable("infos",recyclerState);
        state.putParcelableArrayList("donnees",contacts);
    }

    protected void onRestoreInstanceState(Bundle state){
        super.onRestoreInstanceState(state);
        if (state!=null){
            recyclerState = state.getParcelable("infos");
            contacts = state.getParcelableArrayList("donnees");
            myAdapter.setContacts(contacts);
            myAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPause() {

        super.onPause();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String sauvegardeContacts = new Gson().toJson(contacts);
        Log.d("MesLogs",sauvegardeContacts);
        editor.putString("lescontacts",sauvegardeContacts).apply();
    }
}
