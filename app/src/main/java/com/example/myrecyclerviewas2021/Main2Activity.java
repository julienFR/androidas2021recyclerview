package com.example.myrecyclerviewas2021;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.myrecyclerviewas2021.Model.Contact;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    @Override
    public void finish(){
        EditText nom = findViewById(R.id.edtNom);
        EditText prenom = findViewById(R.id.edtPrenom);
        EditText mail = findViewById(R.id.edtemail);
        EditText ville = findViewById(R.id.edVille);

        Contact contact = new Contact(nom.getText().toString(),
                prenom.getText().toString(),
                mail.getText().toString(),
                ville.getText().toString());
        Intent intent = new Intent();
        intent.putExtra("contact",contact);
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }


}
