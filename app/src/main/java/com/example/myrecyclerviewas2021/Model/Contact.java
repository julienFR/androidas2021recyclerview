package com.example.myrecyclerviewas2021.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Contact implements Parcelable {
    private String nom;
    private String prenom;
    private String email;
    private String ville;

    public Contact(String nom, String prenom, String email, String ville) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.ville = ville;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public static ArrayList<Contact> genererContacts(int nb){
        ArrayList<Contact> res = new ArrayList<Contact>();
        for (int i=0;i<nb;i++){
            Contact contact = new Contact("Nom"+i,"Prenom"+i,
                    "email"+i+"@lemail.com","Ville"+i);
            res.add(contact);
        }
        return res;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nom);
        parcel.writeString(prenom);
        parcel.writeString(email);
        parcel.writeString(ville);
    }

    public Contact(Parcel parcel){
        this.nom = parcel.readString();
        this.prenom = parcel.readString();
        this.email = parcel.readString();
        this.ville = parcel.readString();
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel parcel) {
            return new Contact(parcel);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size] ;
        }
    };
}
