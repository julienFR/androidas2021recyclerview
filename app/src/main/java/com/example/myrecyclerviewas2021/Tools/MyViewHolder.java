package com.example.myrecyclerviewas2021.Tools;


import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.example.myrecyclerviewas2021.Model.Contact;
import com.example.myrecyclerviewas2021.R;

public class MyViewHolder extends ViewHolder implements View.OnLongClickListener {
    private TextView tvFirstName;
    private TextView tvLastName;
    private TextView tvEmail;
    private TextView tvTown;
    private  Contact current;
    ItemLongClickListener itemLongClickListener;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
        tvFirstName = itemView.findViewById(R.id.tvNom);
        tvLastName = itemView.findViewById(R.id.tvPrenom);
        tvEmail = itemView.findViewById(R.id.tvEmail);
        tvTown = itemView.findViewById(R.id.tvVille);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new AlertDialog.Builder(view.getContext()))
                        .setTitle("Infos Contact")
                        .setMessage(current.getNom()+" "+
                                current.getPrenom()+" \n"+
                                "email : "+ current.getEmail()+ " \n"+
                                "ville : "+ current.getVille())
                        .show();
            }
        });
        itemView.setOnLongClickListener(this);
    }

    public void display(Contact contact){
        tvFirstName.setText(contact.getNom());
        tvLastName.setText(contact.getPrenom());
        tvEmail.setText(contact.getEmail());
        tvTown.setText(contact.getVille());
        current = contact;
    }

    @Override
    public boolean onLongClick(View view) {
        this.itemLongClickListener.onItemLongClick(view,getLayoutPosition());
        return false;
    }

    public void setItemLongClickListener(ItemLongClickListener itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }
}
