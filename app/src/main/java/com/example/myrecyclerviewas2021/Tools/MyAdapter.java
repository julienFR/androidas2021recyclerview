package com.example.myrecyclerviewas2021.Tools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myrecyclerviewas2021.Model.Contact;
import com.example.myrecyclerviewas2021.R;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {

    private static ArrayList<Contact> contacts;

    public MyAdapter(ArrayList<Contact> c) {
        contacts = c;
    }

    @NonNull
    @Override
    //permet de creer des nouvelles vues pour la recyclerview
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.cells,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    //permet de remplacer le contenu d'une vue
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            Contact c =contacts.get(position);
            holder.display(c);
            holder.setItemLongClickListener(new ItemLongClickListener() {
                @Override
                public void onItemLongClick(View view, int pos) {
                    contacts.remove(pos);
                    notifyDataSetChanged();
                }
            });
    }

    @Override
    //renvoie le nombre d'elements de la recyclerview
    public int getItemCount() {
        return contacts.size();
    }

    public void setContacts(ArrayList<Contact> c){
        contacts = c;
    }
}
