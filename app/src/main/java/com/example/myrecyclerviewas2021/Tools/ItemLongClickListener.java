package com.example.myrecyclerviewas2021.Tools;

import android.view.View;

public interface ItemLongClickListener {
    void onItemLongClick(View view, int pos);
}
